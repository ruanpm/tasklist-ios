//
//  ViewController.swift
//  SimpleTableView
//
//  Created by Ruan Porto Marques on 29/08/16.
//  Copyright © 2016 Ruan Porto Marques. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var valueToPass: String!
    static var taskList: [Task] = []
    
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var fieldTitle: UITextField!
    @IBOutlet weak var fieldDesc: UITextField!
    @IBOutlet weak var fieldDtLimit: UITextField!
    @IBOutlet weak var fieldResp: UITextField!
    
    
    //Button SAVE action to save new task
    @IBAction func btnSaveTask(sender: AnyObject) {

        //Get values from form fields
        let DtCreation = "01/01/2016"//TODO - get the current date
        let title = fieldTitle.text!
        let desc = fieldDesc.text!
        let dtLimit = fieldDtLimit.text!
        let resp = fieldResp.text!

        let tsk = Task(dtCreation:DtCreation,
                       title: title,
                       desc: desc,
                       dtLimit: dtLimit,
                       resp: resp)
        
        //Add the new task to the list
        ViewController.taskList.append(tsk)
        print(ViewController.taskList)
        TableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("homeCell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.text = ViewController.taskList[indexPath.row].title
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ViewController.taskList.count
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "openDetails"){
            // initialize new view controller and cast it as your view controller
            let detailsController = segue.destinationViewController as! DetailsController
            // store value in the destination view controller attribute
            
            let indexPath = TableView.indexPathForSelectedRow!
            //let currentCell = TableView.cellForRowAtIndexPath(indexPath)
            //valueToPass = (currentCell?.textLabel?.text)!
            //detailsController.passedValue = valueToPass
            
            //Send the Task object referent to the taped item in the list
            detailsController.tsk = ViewController.taskList[indexPath.row]
            
        }
    }
    
    
//    //Perform action of opening a new Controller and show the data related to the cell taped
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        //get cell label
////        let indexPath = tableView.indexPathForSelectedRow()
//        let currentCell = tableView.cellForRowAtIndexPath(indexPath)
//        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        var detailsController = storyboard.instantiateViewControllerWithIdentifier("DetailsController") as! DetailsController
//        detailsController.passedValue = (currentCell?.textLabel?.text)!
//        self.presentViewController(detailsController, animated: true, completion: nil)
//        
//    }

}

