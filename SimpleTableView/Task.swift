//
//  Task.swift
//  SimpleTableView
//
//  Created by Ruan Porto Marques on 29/08/16.
//  Copyright © 2016 Ruan Porto Marques. All rights reserved.
//

import Foundation


class Task{
    
    var dtCreation: String
    var title: String
    var desc: String
    var dtLimit: String
    var resp: String
    
    init(dtCreation: String, title: String, desc: String, dtLimit: String, resp: String){
        self.dtCreation = dtCreation
        self.title = title
        self.desc = desc
        self.dtLimit = dtLimit
        self.resp = resp
    }
    
    init(){
        self.dtCreation = ""
        self.title = ""
        self.desc = ""
        self.dtLimit = ""
        self.resp = ""
    }
}
