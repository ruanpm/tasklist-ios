//
//  DetailsController.swift
//  SimpleTableView
//
//  Created by Ruan Porto Marques on 29/08/16.
//  Copyright © 2016 Ruan Porto Marques. All rights reserved.
//

import Foundation
import UIKit

class DetailsController : UIViewController{
    
    @IBOutlet weak var LabelDtCreation: UILabel!
    @IBOutlet weak var LabelTitle: UILabel!
    @IBOutlet weak var LabelDesc: UILabel!
    @IBOutlet weak var LabelLimitDt: UILabel!
    @IBOutlet weak var LabelResp: UILabel!
    
    var tsk: Task = Task()
    
    override func viewDidLoad() {
        
        print("This is the DetailsController")
        print(tsk)
        
        LabelDtCreation.text = tsk.dtCreation
        LabelTitle.text = tsk.title
        LabelDesc.text = tsk.desc
        LabelLimitDt.text = tsk.dtLimit
        LabelResp.text = tsk.resp
    }
}