//
//  TaskMng.swift
//  SimpleTableView
//
//  Created by Ruan Porto Marques on 29/08/16.
//  Copyright © 2016 Ruan Porto Marques. All rights reserved.
//

import Foundation

class TaskMng{
    var listTasks =  [Task]()
    
    func addTask(tsk: Task){
        listTasks.append(tsk)
    }
    
    func showTasks(){
        for task in listTasks{
            print(task.dtLimit)
        }
    }
    
    func findTaskByTitle( pTitle: String) -> String{
        for task in listTasks{
            if task.title == pTitle{
                return "found it"
            }
        }
        
        return "not found"
    }
    
    func orderByDtLimit(){
        listTasks.sortInPlace({ $0.dtLimit > $1.dtLimit })
    }
}

